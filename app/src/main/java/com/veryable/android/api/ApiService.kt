package com.veryable.android.api
import com.veryable.android.model.Account
import  retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/veryable.json")
    suspend fun getAccounts():Response<List<Account>>
}