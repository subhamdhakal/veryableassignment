package com.veryable.android.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.veryable.android.R
import com.veryable.android.databinding.ItemAccountBinding
import com.veryable.android.model.Account

class PayoutsListAdapter(var context: Context) : RecyclerView.Adapter<PayoutsListAdapter.DayAdapterViewHolder>() {
    lateinit var listener:OnItemClickListener
    interface OnItemClickListener {
        fun onItemClick(item: Account)
    }
    fun setOnItemClickListener( onItemClickListener: OnItemClickListener){
        listener=onItemClickListener
    }
    private var account: List<Account> = listOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayAdapterViewHolder {
        var inflater= LayoutInflater.from(parent.context)
        val binding= ItemAccountBinding.inflate(inflater)
        return DayAdapterViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return account.size
    }
    override fun onBindViewHolder(holder: DayAdapterViewHolder, position: Int) = holder.bind(account[position])


    fun setData(account: List<Account>) {
        this.account = account
        notifyDataSetChanged()
    }

    inner class DayAdapterViewHolder(val binding: ItemAccountBinding): RecyclerView.ViewHolder(binding.root),View.OnClickListener {
        lateinit var localAccount:Account
       fun bind(item: Account) {
            binding.account = item
            localAccount=item
            binding.executePendingBindings()
           if (item.account_type=="card"){
               binding.imageView.setImageResource(R.drawable.baseline_credit_card_black_48pt_3x)
               binding.textViewDetails.text=context.getString(R.string.transfer_type_card)

           }else{
               binding.imageView.setImageResource(R.drawable.baseline_account_balance_black_48pt_3x)
               binding.textViewDetails.text=context.getString(R.string.transfer_type_bank)
           }

        }
        init {
            binding.layoutItem.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
            listener.onItemClick(localAccount)
        }
    }
}