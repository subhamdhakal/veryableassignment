package com.veryable.android.model

data class Account(
    val id : Int,
    val account_type:String,
    val account_name: String,
    val desc:String
)
