package com.veryable.android.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.veryable.android.R
import com.veryable.android.adapter.PayoutsListAdapter
import com.veryable.android.api.ApiService
import com.veryable.android.api.RetrofitHelper
import com.veryable.android.databinding.ActivityPayoutsListBinding
import com.veryable.android.model.Account
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import retrofit2.HttpException

class PayoutsListActivity : AppCompatActivity() {

    private lateinit var binding : ActivityPayoutsListBinding
    private lateinit var linearLayoutManagerBank: LinearLayoutManager
    private lateinit var linearLayoutManagerCard: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payouts_list)
        linearLayoutManagerBank = object:LinearLayoutManager(this){
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        linearLayoutManagerCard = object:LinearLayoutManager(this){
            override fun canScrollVertically(): Boolean {
                return false
            }
        }
        binding.recyclerView.layoutManager=linearLayoutManagerBank
        binding.recyclerViewCard.layoutManager=linearLayoutManagerCard
        supportActionBar!!.hide()
        var payoutsListAdapterBank= PayoutsListAdapter(this)
        var payoutsListAdapterCard= PayoutsListAdapter(this)

        payoutsListAdapterBank.setOnItemClickListener(object: PayoutsListAdapter.OnItemClickListener{
            override fun onItemClick(item: Account) {
                val intent = Intent(this@PayoutsListActivity,DetailsActivity::class.java)
                intent.putExtra("account_name",item.account_name)
                intent.putExtra("desc",item.desc)
                startActivity(intent)
            }
        })
        payoutsListAdapterCard.setOnItemClickListener(object: PayoutsListAdapter.OnItemClickListener{
            override fun onItemClick(item: Account) {
                val intent = Intent(this@PayoutsListActivity,DetailsActivity::class.java)
                intent.putExtra("account_name",item.account_name)
                intent.putExtra("desc",item.desc)
                startActivity(intent)
            }
        })
        binding.recyclerView.adapter=payoutsListAdapterBank
        binding.recyclerViewCard.adapter=payoutsListAdapterCard
        val quotesApi = RetrofitHelper.getInstance().create(ApiService::class.java)
        CoroutineScope(Dispatchers.IO).launch {
            val response = quotesApi.getAccounts()
            withContext(Dispatchers.Main) {
                try {
                    if (response.isSuccessful) {

                        var dataBank=response.body()?.filter { item -> item.account_type=="bank"  }
                        response.body()?.let {
                            if (dataBank != null) {
                                payoutsListAdapterBank.setData(dataBank)
                            }
                        }
                        var dataCard=response.body()?.filter { item -> item.account_type=="card"  }
                        response.body()?.let {
                            if (dataCard != null) {
                                payoutsListAdapterCard.setData(dataCard)
                            }
                        }
                    } else {
                        //toast("Error: ${response.code()}")
                        Log.e("error",response.code().toString())
                    }
                } catch (e: HttpException) {
                    Log.e("error",response.code().toString())
                } catch (e: Throwable) {
                    Log.e("error",response.code().toString())
                }
            }
        }
    }


}