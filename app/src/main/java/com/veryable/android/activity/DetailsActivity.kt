package com.veryable.android.activity

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.veryable.android.R
import com.veryable.android.databinding.ActivityDetailsBinding


class DetailsActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        binding.textView.text=intent.getStringExtra("account_name").toString()
        binding.textView2.text=intent.getStringExtra("desc").toString()
        supportActionBar!!.hide()
        binding.buttonBack.setOnClickListener{
            finish()
        }
        binding.buttonDone.setOnClickListener {
            finish()
        }

    }
}